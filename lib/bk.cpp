// bk.cpp

#include "bk.h"

using namespace bk;

// Dd

Str Dd::str( const Acc &da, const Acc &ca ) const {
  if( ! da.o.currency && ! ca.o.currency ) return n2s( d );
  return n2s( d ) + da.currency() + "; " + n2s( c ) + ca.currency();
}

bool Dd::isD( const Acc &a ) const {
  return a.o.type == Acc::Active || ( a.o.type == Acc::ActPass && d >= c );
}

Str Dd::str( const Acc &a ) const {
  if( isD( a )) return n2s( d - c ) + ";";
  return "(; " + n2s( c - d ) + ")";
}

Str Dd::tds( const Acc &a ) const {
  if( isD( a ))
    return "<td align=\"right\">" + n2s( d - c ) + "</td><td></td>";
  return "<td></td><td align=\"right\">" + n2s( c - d ) + "</td>";
}

// Sld

Str Sld::str( bool printTime ) const {
  return ( printTime ? t.str() + "; " : Str())
         + a.str() + "; = " + s.str( a );
}

Str Sld::tds( bool printTime ) const {
  return ( printTime ? "<td>" + t.str() + "</td>" : Str())
         + "<td>" + a.str() + "</td>" + s.tds( a );
}


const char *tableStyle = R"(<meta charset="UTF-8">
<style>
  * { font-family: Ubuntu; }
  td { border-left: 1px solid gray; padding: 2px 0.5em; }
  table { border-collapse: collapse; }
</style>
)";

struct HtmlTable {
  HtmlTable( std::ostream &o, const Str &caption ) : o( o ) {
    o << tableStyle << "<table><caption>" << caption << "</caption>\n";
  }
  ~HtmlTable() { thead0(); tbody0(); o << "</table>\n"; }
  HtmlTable &thead() { o << "<thead>"; Thead = true; return *this; }
  HtmlTable &tbody() { thead0(); o << "<tbody>"; Tbody = true; return *this; }
  HtmlTable &tr( const Str &s = Str()) { tr0(); o << "<tr>" << s; Tr = true; return *this; }
  HtmlTable &th( const Str &s = Str()) { o << "<th>" << s << "</th>"; return *this; }
  HtmlTable &td( const Str &s = Str(), bool right = false ) {
    o << "<td"; if( right ) o << " align=\"right\""; o << ">" << s << "</td>";
    return *this;
  }
  HtmlTable &td( double d, bool right = true ) { return td( n2s( d ), right ); }
  HtmlTable &txt( const Str &s ) { o << s; return *this; }
protected:
  void tr0() { if( Tr ) o << "</tr>\n"; Tr = false; }
  void thead0() { tr0(); if( Thead ) o << "</thead>\n"; Thead = false; }
  void tbody0() { tr0(); if( Tbody ) o << "</tbody>\n"; Tbody = false; }
  bool Thead = false, Tbody = false, Tr = false;
  std::ostream &o;
};

struct OFile : std::ofstream {
  using std::ofstream::ofstream;
  OFile( const Str& fName ) : std::ofstream( fName ) {
    if( ! good()) throw std::runtime_error( "Cannot write to: " + fName );
  }
};

void saldos( Time t = cfg().periodEnds, bool savedOnly = false ) {
  std::map<Acc::ConstRef, Sld, Acc::AccCmp> saldos;
  for( const Acc &a: Acc::accs ) {
    Sld sld = savedOnly ? Sld::findSaved( a, t ) : En::saldo( a, t );
    saldos.insert( { sld.a, sld } );
  }
  Str name = savedOnly ? "savedSaldos" : "saldos",
      caption = name + '(' + t.str() + ')',
      fName = cfg().outputDirectory + name + cfg().outFileNameSuffix;
  {
    OFile o( fName + ".html" );
    HtmlTable t( o, caption );
    t.thead().tr(); if( savedOnly ) t.th( "Time" );
    t.th( "Acount" ).th( "Debet" ).th( "Credit" ).tbody();
    for( const auto &s: saldos ) t.tr( s.second.tds( savedOnly ));
  }
  {
    OFile o( fName + ".txt" );
    o << caption << '\n';
    for( const auto &s: saldos )
      o << s.second.str( savedOnly ) << '\n';
  }
}

static Str timePeriod( Time t1, Time t2 ) {
  if( cfg().periodHumanly ) t1 = static_cast<std::time_t>( t1 ) + 1 /* одна секунда */;
  return utl::f( "{1}{2} .. {3}]", cfg().periodHumanly ? "[" : "(", t1.str(), t2.str());
}

void turnover( Time t1 = cfg().periodBegins, Time t2 = cfg().periodEnds,
  const Str &fName = "turnover" )
{
  OFile o( cfg().outputDirectory + fName + cfg().outFileNameSuffix + ".html" );

  HtmlTable t( o, "Turnover " + timePeriod( t1, t2 ));
  t.thead().tr().th( "Acc" ).th( "DSaldo1" ).th( "CSaldo1" )
  .th( "Debet" ).th( "Credit" ).th( "DSaldo2" ).th( "CSaldo2" )
  .th( "Delta" ).tbody();

  for( const Acc &a: Acc::accs ) {
    Sld sld1 = En::saldo( a, t1 ), sld2 = En::saldo( a, t2 );
    To to = En::turnover( a, t1, t2 );
    double delta = ( sld2.s.d - sld2.s.c ) - ( sld1.s.d - sld1.s.c )
                 - ( to.d - to.c );

    t.tr().td( a.str()).txt( sld1.s.tds( a )).td( to.d ).td( to.c )
     .txt( sld2.s.tds( a ))
     .td( std::abs( delta ) < 0.01 ? Str() : n2s( delta ), true );
  }
}

// t1 < entries <= t2
void entries( Time t1 = cfg().periodBegins, Time t2 = cfg().periodEnds,
  const Str &fName = "entries" )
{
  OFile o( cfg().outputDirectory + fName + cfg().outFileNameSuffix + ".html" );
  HtmlTable t( o, "Entries " + timePeriod( t1, t2 ));
  t.thead().tr()
   .th( "Time" ).th( "Debet" ).th( "Credit" ).th( "Sum" ).th( "Rem" )
   .tbody();
  for( auto f = En::upperBound( t1 ); f != En::ens.end() && f->t <= t2; f ++ )
    t.tr( f->tds());
}

void chess( Time t1 = cfg().periodBegins, Time t2 = cfg().periodEnds,
  const Str &fName = "chess" )
{
  // сначала найдем счета, по которым был оборот в отчетный период
  std::set<Acc::ConstRef, Acc::AccCmp> accsD, accsC;
  for( auto f = En::upperBound( t1 ); f != En::ens.end() && f->t <= t2; f ++ ) {
    accsD.insert( f->d );
    accsC.insert( f->c );
  }

  OFile o( cfg().outputDirectory + fName + cfg().outFileNameSuffix + ".html" );
  if( ! accsD.size()) { // empty table
    o << "Empty (chess) table " << timePeriod( t1, t2 );
    return;
  }
  HtmlTable t( o, "Chess " + timePeriod( t1, t2 ));
  t.thead().tr().th();
  for( const Acc &a: accsC )
    t.th( a.str());
  t.th().tbody();

  std::map<Acc::ConstRef, double, Acc::AccCmp> totalc;
  for( const Acc &a: accsD ) {
    std::map<Acc::ConstRef, Dd, Acc::AccCmp> line; double totald = 0.0;
    for( auto f = En::upperBound( t1 ); f != En::ens.end() && f->t <= t2; f ++ )
      if( f->d == a ) {
        Dd v = { f->v };
        line[ f->c ] += v; totald += v.d; totalc[ f->c ] += v.c;
      }
    t.tr().td( a.str()); // первая ячейка
    for( const Acc &ca: accsC ) {
      Dd v = line[ ca ];
      t.td( ( v.d == v.c && v.d == 0 ) ? Str() : v.str( a, ca ), true );
    }
    t.td( totald ); // последняя ячейка
  }
  t.tr().td();
  for( const Acc &a: accsC )
    t.td( totalc[ a ] );
}

int main() try {
  SqVm v; bk::squirrels::registerSquirrels( v );
  bk::initCfg       ( v, "bk++.cfg" );
  bk::Acc::initAccs ( v );
  bk::Sld::initSSlds( v );
  bk::En::initEns   ( v );

  if( ! cfg().periodHumanly) std::cout << R"( Предупреждение:
    Остатки на заданный момент времени t рассчитываются так, что в остаток
    входят все операции, время t(en) которых меньше или равно заданному
    моменту времени t (нестрогое неравенство t(en) <= t).

    Обороты на заданный период времени (t1;t2] рассчитываются так, что в
    оборот входят все операции, момент времени t(en) которых больше (строгое
    неравенство t(en) > t1) заданного момента времени начала периода t1,
    и меньше или равно (нестрогое неравенство t(en) <= t2) моменту времени
    конца заданного периода t2. То есть: t1 < t(en) <= t2.

)";

    if( cfg().reportSavedSaldos ) saldos  ( cfg().periodEnds, true /* только сохраненные */ );
    if( cfg().reportSaldos )      saldos  ();
    if( cfg().reportTurnover )    turnover();
    if( cfg().reportEntries )     entries ();
    if( cfg().reportChess )       chess   ();

    if( ! cfg().scriptFile.empty()) SqFile( v, cfg().scriptFile ).run();

  if( cfg().verbose >= Config::LogDebug ) {
    printf( "sizeof(Acc)=%lu\n", sizeof(Acc));
    printf( "sizeof(En)=%lu\n", sizeof(En));
  }
  return 0;
} catch ( const std::exception &e )
  { std::cerr << "bk++ error: " << e.what() << std::endl; return 1; }

