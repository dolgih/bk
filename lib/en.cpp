// en.cpp

#include "bk.h"

namespace bk {

Str En::tds() const {
  return "<td>" + t.str() + "</td><td>" + d.str() + "</td><td>" + c.str()
       + "</td><td align=\"right\">" + v.str( d, c ) + "</td><td>" + rem
       + "</td>";
}

Sld En::saldo( const Acc &a, Time t ) {
  Sld sld = Sld::findSaved( a, t );
  auto fnd = ens.upper_bound( { sld.t, a } );
  for( ; fnd != En::ens.end() && fnd->t <= t; fnd ++ ) {
    if( fnd->d == a ) sld.s += Dd( fnd->v.d, 0.0 );
    if( fnd->c == a ) sld.s += Dd( 0.0, fnd->v.c );
  }
  sld.t = t;
  return sld;
}

To En::turnover( const Acc &a, Time t1, Time t2 ) {
  To to( t1, t2, a );
  auto fnd = En::upperBound( t1 );
  for( ; fnd != En::ens.end() && fnd->t <= t2; fnd ++ ) {
    if( fnd->d == a ) to.d += fnd->v.d;
    if( fnd->c == a ) to.c += fnd->v.c;
  }
  return to;
}

To En::turnover( const Acc &a, Time t1, Time t2, const Acc &corr ) {
  To to( t1, t2, a );
  auto fnd = En::upperBound( t1 );
  for( ; fnd != En::ens.end() && fnd->t <= t2; fnd ++ ) {
    if( fnd->d == a && fnd->c == corr ) to.d += fnd->v.d;
    if( fnd->c == a && fnd->d == corr ) to.c += fnd->v.c;
  }
  return to;
}

} // bk

