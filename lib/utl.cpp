// utl.cpp

#include "bk.h"

namespace bk {
namespace utl {

// F

F::operator Str() const {
  Str r = theFormat;
  for( const auto &i: store ) r = replace( r, '{' + n2s( i.first + 1 ) + '}', i.second );
  return r;
}

// string functions

// Returns a new string in which all occurrences of a specified string
// in the current instance are replaced with another specified string.
Str replace( Str s, Str pattern, Str repl ) {
  size_t index = 0;
  for(;;) {
    /* Locate the substring to replace. */
    index = s.find( pattern, index );
    if( index == Str::npos ) break;

    /* Make the replacement. */
    s.replace( index, pattern.length(), repl );

    /* Advance index forward so the next iteration doesn't pick it up as well. */
    index += repl.length();
  }
  return s;
}

}} // utl, bk

