// a.cpp

#include "bk.h"

namespace bk {

Acc::Accs Acc::accs;
Acc::AccsByName Acc::accsByName;

static const char RUR[] = "RUR", USD[] = "USD";
typedef std::map<Str, unsigned short> CurrenciesByName;
CurrenciesByName currenciesByName = { { RUR, 0 }, { USD, 1 }};
const char *Acc::currency() const {
  // TODO неэффективно очень сделано линейным поиском
  for( const auto &c: currenciesByName )
    if( c.second == o.currency ) return c.first.data();
  return "*error: Currency not found*";
}

struct Currencies : ReadSq {
  Currencies( SqVm &v ) : ReadSq( v, cfg().inputDirectory + "Currencies.constants",
                                  "Currencies" ), v( v ) { }
  void run() override {
    ReadSq::run();
    sq_pushconsttable( v );
    if( cfg().verbose >= Config::LogInfo ) std::cerr << "Constants list:\n";
    for( const auto &c: currenciesByName ) {
      if( cfg().verbose >= Config::LogInfo )
        std::cerr << "currency=" << c.first << "; num=" << c.second << '\n';
      v.newslot( c.first, c.second );
    }
    sq_poptop( v /* consttable */ );
  }
  void line() override {
    // here -1 is the value and -2 is the key
    if( sq_gettype( v, -1 ) != OT_STRING )
      error( "not string: Currency" );
    Str s = v.get<const SQChar*>( -1 ); for( auto &c: s ) c = toupper( c );
    if( currenciesByName.find( s ) != currenciesByName.end()) {
      if( s == RUR || s == USD ) return;
      else error( "duplicate currency: " + s );
    }
    currenciesByName[ s ] = currenciesByName.size();
  }
  SqVm &v;
};

static const char *const ActPass = "ActPass", *const Active = "Active",
                  *const Passive = "Passive";
const char *Acc::type() const {
  switch( o.type ) {
    case ActPass: return bk::ActPass;
    case Active:  return bk::Active;
    case Passive: return bk::Passive;
    default: return "*account type error*";
  }
}

struct Accounts : ReadSq {
  Accounts( SqVm &v )
    : ReadSq( v, cfg().inputDirectory + "Accounts.constants", "Accounts" ), v( v ) { }
  void run() override {
    // Определим константы ActPass, Active, Passive
    sq_pushconsttable( v );
    v.newslot( ActPass, ActPass );
    v.newslot( Active,  Active );
    v.newslot( Passive, Passive );
    sq_poptop( v /* consttable */ );

    sq_newtable( v ); // new Acc table

    ReadSq::run();

    /* Замена введенного пользователем массива Accounts на
       таблицу объектов класса Acc */
    sq_pushroottable( v );
    v.push( global );
    sq_push( v, -3 /* the copy of new Acc table */ );
    sq_set( v, -3 );
    sq_pop( v, 2 /* roottable and new Acc table */ );

    sq_pushconsttable( v );
    if( cfg().verbose >= Config::LogInfo ) std::cerr << "Constants list:\n";
    for( const Acc &a: Acc::accs ) {
      if( cfg().verbose >= Config::LogInfo ) std::cerr << a.str() << '\n';
      v.newslot( a.n, a.n );
    }
    sq_poptop( v /* consttable */ );
  }
  void line() override {
    // here -1 is the value and -2 is the key
    if( sq_gettype( v, -1 ) != OT_ARRAY ) error( "not array: Accounts" );
    // array
    if( sq_getsize( v, -1 ) < 3 ) error( "too little array" );
    int a, s; Str n; Acc::Options o;

    a = v.find<SQInteger>( 0, "Acc::a" );
    s = v.find<SQInteger>( 1, "Acc::s" );
    n = v.find<const SQChar*>( 2, "Acc::s" ); for( auto &c: n ) c = tolower( c );

    if( v.findType( 3 ) == OT_INTEGER ) o = v.find<SQInteger>( 3, "Acc::o" );
    else if( v.findType( 3 ) == OT_ARRAY ) {
      v.push( 3 ); sq_get( v, -2 );
      o.currency = v.find<SQInteger>( 0, "Acc::o::currency" );
      Str type = v.find<const SQChar*>( 1, "Acc::o::type");
      if( type == Active ) o.type = Acc::Active;
      else if( type == Passive ) o.type = Acc::Passive;
      else if( type == ActPass ) o.type = Acc::ActPass;
      else error( "bad Acc::option::type" );
      sq_poptop( v /* array */ );
    } else error( "bad Acc::option" );

    if( Acc::findByName( n ) != Acc::accsByName.end())
      error( "duplicate account: " + n );
    Acc::accsByName.emplace( a, s, n, o );

    if( ! cfg().scriptFile.empty()) {
      /* Замена введенного пользователем массива Accounts на
         таблицу объектов класса Acc */
      v.push( n /* key */ );
      v.push( n /* key for constructor */ );
      v.createInstance( squirrels::Acc::typeOf() /* value */, 1 );
      if( SQ_FAILED( sq_newslot( v, -8, 0 )))
        error( "sq_newslot(Acc)" );
    }
  }
  SqVm &v;
};

void Acc::initAccs( SqVm &v ) { Currencies( v ).run(); Accounts( v ).run(); }

} // bk

