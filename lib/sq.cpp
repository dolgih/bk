// sq.cpp

#include "bk.h"

namespace bk {

// SqVm

SqVm::SqVm() : v( sq_open( 1024 )) {
  // REGISTRATION OF STDLIB
//  sq_pushroottable( v ); //push the root table where the std function will be registered
//  sqstd_register_iolib( v );  //registers a library
//  sqstd_register_systemlib( v );
//  sqstd_register_stringlib( v );
  // ... call here other stdlibs string,math etc...
//  sq_poptop( v /* pops the root table */ );
  // END REGISTRATION OF STDLIB

  sq_setforeignptr( v, this );
  sqstd_seterrorhandlers( v ); //registers the default error handlers
  sq_setprintfunc( v, printfunc, errorfunc ); //sets the print function
}

void SqVm::printStats() {
  if( cfg().verbose < Config::LogDebug ) return;
  std::cerr << "sq_gettop(v) =" << sq_gettop( v ) << '\n';
  sq_pushroottable( v ); // globals
  sq_pushconsttable( v );
  std::cout << "Size of root table =" << sq_getsize( v, -2 ) << '\n';
  std::cout << "Size of const table =" << sq_getsize( v, -1 ) << '\n';
  sq_pop( v, 2 /* pops the consttable and roottable */ );
}

void SqVm::get( Time &t, SQInteger idx ) const {
  if( sq_gettype( v, idx ) == OT_STRING )
    t = get<const SQChar*>( idx );
  else if( sq_gettype( v, idx ) == OT_ARRAY ) {
    int sz = sq_getsize( v, idx );
    if( sz < 5 && sz != 3 )
      throw std::runtime_error( "SqVm::getTime: bad time array" );
    SQInteger ta[ 6 ];
    for( int k = 0; k < 6; k ++ )
      ta[ k ] = k < sz ? find<SQInteger>( k, idx, "Time:" + n2s( k )) : 0;
    t = Time( ta[ 0 ], ta[ 1 ], ta[ 2 ], ta[ 3 ], ta[ 4 ], ta[ 5 ] );
  } else
    if( sq_gettype( v, idx ) == OT_INSTANCE ) {
      if( auto *p = dynamic_cast<squirrels::Tm*>( get<SquirrelableBase*>( idx )))
        t = static_cast<const std::tm &>( *p );
      else
        throw std::runtime_error( "SqVm::getTime: not Tm class" );
  } else
    throw std::runtime_error( "SqVm::getTime: not Time" );
}

SQObjectType SqVm::findType( SQInteger idx ) const {
  push( idx ); sq_get( v, -2 );
  SQObjectType t = sq_gettype( v, -1 );
  sq_poptop( v );
  return t;
}

void SqVm::createInstance( Str cls, SQInteger argc ) {
  sq_pushroottable( v );
  push( cls );
  if( SQ_FAILED( sq_get( v, -2 ))) // class
    throw std::runtime_error( "createInstance(): no such class" );
  sq_remove( v, -2 /* roottable */ );
  if( SQ_FAILED( sq_createinstance( v, -1))) // instance
    throw std::runtime_error( "createInstance(): sq_createinstance" );
  sq_remove( v, -2 /* class */ );
  push( "constructor" );
  if( SQ_FAILED( sq_get( v, -2 ))) // constructor from instance
    throw std::runtime_error( "createInstance(): no constructor" );

  sq_push( v, -2 /* instance */ );
  for( int i = 0; i < argc; i ++ ) {
    sq_push( v, -3 - argc /* args */ );
    sq_remove( v, -4 - argc /* args */ );
  }
  if( SQ_FAILED( sq_call( v, 1 + argc, SQFalse, SQTrue )))
    throw std::runtime_error( "createInstance(): constructor call" );
  sq_remove( v, -1 /* constructor */ );
}

Str SqVm::lastError() const {
  errMsg = "";
  const SQChar *error;
  sq_getlasterror( v );
  if ( SQ_SUCCEEDED( sq_getstring( v, -1, &error ) ) ) {
    errMsg = errMsg + "sqvm error: " + error;
  }
  return errMsg;
}

void SqVm::printfunc( HSQUIRRELVM v, const SQChar *s, ... ) {
  va_list vl;
  va_start( vl, s );
  vfprintf( stdout, s, vl );
  va_end( vl );
}

void SqVm::errorfunc( HSQUIRRELVM v, const SQChar *s, ... ) {
  va_list vl;
  va_start( vl, s );
  vfprintf( stderr, s, vl );
  va_end( vl );

  va_start( vl, s );
  SqVm *This = static_cast<SqVm*>( sq_getforeignptr( v ));
  const int maxErrMsg = 1024;
  Str errMsg;
  errMsg.reserve( maxErrMsg );
  vsnprintf( ( char* )errMsg.data(), maxErrMsg, s, vl );
  This->errMsg += errMsg.data();
  va_end( vl );
}


// SqFile

void SqFile::run() {
  sq_pushroottable( v ); //push the root table(were the globals of the script will be stored)
  if( SQ_FAILED( sqstd_dofile( v, fName.data(), SQFalse, SQTrue ))) // also prints syntax errors if any
    error( v.lastError());
  sq_poptop( v /* pops the root table */ );
  v.printStats();
}


// ReadSq

void ReadSq::run() {
  // Создание глобальной переменной
  sq_pushroottable( v );
  v.newslot( global, "I'm empty, fill me." );
  sq_poptop( v /* roottable */ );

  SqFile::run();

  sq_pushroottable( v ); // globals
  v.push( global );
  if( SQ_FAILED( sq_get( v, -2 )))
    error( "global not found: " + global );
  if( sq_gettype( v, -1 ) != OT_ARRAY )
    error( "not array: " + global );
  sq_pushnull( v ); // null iterator
  while( SQ_SUCCEEDED( sq_next( v, -2 ))) {
    // here -1 is the value and -2 is the key
    line();
    sq_pop( v, 2 /* pops key and val before the next iteration */ );
  }
  sq_pop( v, 3 /* pops the null iterator, global, roottable */ );
  v.printStats();
}

} // bk

