// sq.h

#pragma once

namespace bk {

class SquirrelableBase;

struct SqVm {
  SqVm();

  void printStats();

  struct Error : std::runtime_error {
    Error( const Str &msg ) : std::runtime_error( "SqVm: " + msg ) { }
  };

  virtual ~SqVm() { if( v ) sq_close( v ); }

  operator HSQUIRRELVM& () { return v; }

  void push( SQInteger i ) const { sq_pushinteger( v, i ); }
  void push( bool b ) const { sq_pushbool( v, b ); }
  void push( SQFloat f ) const { sq_pushfloat( v, f ); }
#ifndef SQUSEDOUBLE
  void push( double d ) const { push( (SQFloat)d ); }
#endif
  void push( const SQChar *c ) const { sq_pushstring( v, c, -1 ); }
  void push( int i ) const { push( (SQInteger)i ); }
  void push( const Str &s ) const { push( (const SQChar *)s.data()); }

  template <class T> void newslot( const Str& key, const T &val ) {
    push( key ); push( val );
    if( SQ_FAILED( sq_newslot( v, -3, 0 )))
      throw Error( "cannot add slot: " + key );
  };

  void get( SquirrelableBase *&s, SQInteger idx ) const {
    if( SQ_FAILED( sq_getinstanceup( v, idx, reinterpret_cast<SQUserPointer*>( &s ), 0 )))
      throw Error( "sq_getinstanceup" );
  }
  void get( Time &t, SQInteger idx ) const;
  void get( SQInteger &i, SQInteger idx ) const {
    if( SQ_FAILED( sq_getinteger( v, idx, &i ))) throw Error( "sq_getinteger" );
  }
  void get( SQBool &b, SQInteger idx ) const {
    if( SQ_FAILED( sq_getbool( v, idx, &b ))) throw Error( "sq_getbool" );
  }
  void get( SQFloat &f, SQInteger idx ) const {
    if( SQ_FAILED( sq_getfloat( v, idx, &f ))) throw Error( "sq_getfloat" );
  }
  void get( const SQChar *&c, SQInteger idx ) const {
    if( SQ_FAILED( sq_getstring( v, idx, &c ))) throw Error( "sq_getstring" );
  }
  template <class T> T get( SQInteger idx ) const { T g; get( g, idx ); return g; }
  template <class T> T get( SQInteger idx, const Str &n ) const {
    try { return get<T>( idx ); }
    catch( std::exception &e )
    { throw std::runtime_error( Str( e.what()) + ": " + n ); }
  }

  // получение данных из элемента массива или таблицы
  SQObjectType findType( SQInteger idx ) const;
  template <class T, class I> T find( I key, const Str &n ) const {
    push( key ); sq_get( v, -2 ); T t = get<T>( -1, n );
    sq_poptop( v ); return t;
  }
  template <class T, class I> T find( I key, SQInteger idx, const Str &n ) const {
    push( key ); sq_get( v, idx < 0 ? idx - 1 : idx ); T t = get<T>( -1, n );
    sq_poptop( v ); return t;
  }

  /** Создает экземпляр класса cls и помещает его в стек,
      перед вызовом в стек должны быть помещены параметры,
      количество параметров argc */
  void createInstance( Str cls, SQInteger argc );

protected:
  HSQUIRRELVM v;
  mutable Str errMsg;

  Str lastError() const;
  static void printfunc( HSQUIRRELVM v, const SQChar *s, ... );
  static void errorfunc( HSQUIRRELVM v, const SQChar *s, ... );

friend struct SqFile;
};

struct SqFile {
  SqFile( SqVm &v, Str fName ) : v( v ), fName( fName ) { }
  virtual void run();

protected:
  [[noreturn]] void error( const Str& msg ) { throw Error( fName, msg ); }
  struct Error : std::runtime_error {
    Error( const Str &fName, const Str &msg ) : std::runtime_error( "SqFile \"" + fName + "\": " + msg ) { }
  };

  SqVm &v; Str fName;
};

struct ReadSq : SqFile {
protected:
  Str global;

  ReadSq( SqVm &v, Str fName, Str global ) : SqFile( v, fName ), global( global ) { }
  void run() override;

  virtual void line() = 0;
};

/* struct SqObject : SQObject {
  SqObject( HSQUIRRELVM v, SQInteger idx ) {
    sq_resetobject( this ); sq_getstackobj( v, idx, this );
  }
}; */

} // bk

