// e.cpp
// bk Entries

#include "bk.h"

namespace bk {

En::Ens En::ens;

struct Entries : ReadSq {
  Entries( SqVm &v )
    : ReadSq( v, cfg().inputDirectory + "Entries.in", "Entries" ), v( v ) { }
  void run() override {
    ReadSq::run();
    if( cfg().verbose >= Config::LogInfo ) {
      std::cerr << "Values list:\n";
      for( const auto &e: En::ens )
        std::cerr << e.str() << '\n';
    }
  }
  void line() override {
    // here -1 is the value and -2 is the key
    if( sq_gettype( v, -1 ) != OT_ARRAY ) error( "not array: Entries" );
    // array
    int sz = sq_getsize( v, -1 );
    if( sz < 4 ) error( "too little array" );
    Time t; Str d, c; Dd s; Str rem;

    t =   v.find<Time>( 0, "En::t" );
    d =   v.find<const SQChar*>( 1, "En::d" );
    c =   v.find<const SQChar*>( 2, "En::c" );
    s.d = v.find<SQFloat>( 3, "En::s.d" );
    bool difCurrencies = false; // различные валюты
    if( sz > 4 ) {
      if( v.findType( 4 ) == OT_STRING ) rem = v.find<const SQChar*>( 4, "En::rem" );
      else s.c = v.find<SQFloat>( 4, "En::s.c" ), difCurrencies = true;
    }
    if( sz > 5 ) rem = v.find<const SQChar*>( 4, "En::rem" );

    auto fndD = Acc::findByName( d );
    if( fndD == Acc::accsByName.end())
      error( "debet account not found" );
    auto fndC = Acc::findByName( c );
    if( fndC == Acc::accsByName.end())
      error( "credit account not found" );
    if( difCurrencies )
      En::ens.emplace( t, *fndD, *fndC, s.d, s.c, rem );
    else
      En::ens.emplace( t, *fndD, *fndC, s.d, rem );
  }
protected:
  SqVm &v;
};

void En::initEns( SqVm &v ) { Entries( v ).run(); }

} // bk

