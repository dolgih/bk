// cfg.h

#pragma once

namespace bk {


struct Config : SqFile {
  Config( SqVm &v, const Str& fName );
  virtual ~Config() { }
  void run() override;

  enum LogLevel { LogError = 0, LogWarning = 1, LogInfo = 2, LogDebug = 3 }
       verbose = LogInfo;
  bool reportSavedSaldos, reportEntries, reportSaldos, reportTurnover,
       reportChess,
       periodHumanly;
  Time periodBegins, periodEnds;
  Str  inputDirectory, outputDirectory, outFileNameSuffix,
       scriptFile;
};

void initCfg( SqVm &v, const Str& fName );
const Config &cfg();


} // bk

