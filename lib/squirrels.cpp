// squirrels.cpp

#include <bk.h>

namespace bk {
namespace squirrels {

// Tm

SQInteger Tm::hlp( HSQUIRRELVM h ) {
sq_pushstring( h, R"(    Tm help:
  Конструктор принимает строку, 3 или 5 или 6 целых, или другой объект Tm.
  Возвращает компоненты времени-даты по индексам: day, mon, year, hour, min,
sec.
  Увеличение-уменьшение покомпонентно на c единиц: addDays(с), addMons(с),
addYears(с), addHours(с), addMins(с), addSecs(с). Все методы возвращают
измененного самого себя.
)", -1 ); return 1;
}

SQInteger Tm::constructor( SqVm &v ) try {
  switch( sq_gettop( v )) { // number of arguments
    case 1: t = Time::now(); break;
    case 2: t = v.get<Time>( 2, "Sq:Tm: Time" ); break;
    case 4: t = Time( v.get<SQInteger>( 2, "Sq:Tm: Time:1" ),
                      v.get<SQInteger>( 3, "Sq:Tm: Time:2" ),
                      v.get<SQInteger>( 4, "Sq:Tm: Time:3" ));
            break;
    case 6: t = Time( v.get<SQInteger>( 2, "Sq:Tm: Time:1" ),
                      v.get<SQInteger>( 3, "Sq:Tm: Time:2" ),
                      v.get<SQInteger>( 4, "Sq:Tm: Time:3" ),
                      v.get<SQInteger>( 5, "Sq:Tm: Time:4" ),
                      v.get<SQInteger>( 6, "Sq:Tm: Time:5" ));
            break;
    case 7: t = Time( v.get<SQInteger>( 2, "Sq:Tm: Time:1" ),
                      v.get<SQInteger>( 3, "Sq:Tm: Time:2" ),
                      v.get<SQInteger>( 4, "Sq:Tm: Time:3" ),
                      v.get<SQInteger>( 5, "Sq:Tm: Time:4" ),
                      v.get<SQInteger>( 6, "Sq:Tm: Time:5" ),
                      v.get<SQInteger>( 7, "Sq:Tm: Time:6" ));
            break;
    default: throw std::runtime_error( "Sq:Tm: num args" );
  }
  return 0;
} catch ( std::exception &e ) { return sq_throwerror( v, e.what()); }

SQInteger Tm::get( HSQUIRRELVM h ) try {
  auto &v = getV<SqVm>( h );
  if( sq_gettop( v ) != 2 ) // number of arguments
    throw std::runtime_error( "Sq:Tm:get: num args" );
  auto *i = v.get<const SQChar *>( 2, "Sq:Tm:get: string index" );
  auto l = strlen( i ); if( ! l )
    throw std::runtime_error( "Sq:Tm:get: empty index" );
  auto &t = getO<Tm>( h ).t;
  // возвращает одно целое значение
  if     ( ! strncasecmp( "day",  i, l )) v.push( t.tm_mday );
  else if( ! strncasecmp( "mon",  i, l )) v.push( t.tm_mon + 1 );
  else if( ! strncasecmp( "year", i, l )) v.push( t.tm_year - 100 );
  else if( ! strncasecmp( "hour", i, l )) v.push( t.tm_hour );
  else if( ! strncasecmp( "min",  i, l )) v.push( t.tm_min );
  else if( ! strncasecmp( "sec",  i, l )) v.push( t.tm_sec );
  else throw std::runtime_error( "Sq:Tm:get: bad index" );
  return 1;
} catch ( std::exception &e ) { return sq_throwerror( h, e.what()); }

// Группа функций add...()

#define addTime( name, memb ) \
SQInteger Tm::add ## name ## s( HSQUIRRELVM h ) try { \
  auto &v = getV<SqVm>( h ); \
  if( sq_gettop( v ) != 2 ) /* number of arguments */ \
    throw std::runtime_error( "Sq:Tm:add" # name "s: num args" ); \
  auto &t = getO<Tm>( h ).t; \
  t.tm_ ## memb += v.get<SQInteger>( 2, "Sq:Tm:add" # name "s: integer value to add" ); \
  mktime( &t ); \
  /* возвращает экземляр самого себя (как *this в C++) */ \
  sq_poptop( v ); \
  return 1; \
} catch ( std::exception &e ) { return sq_throwerror( h, e.what()); }

addTime( Day,  mday )
addTime( Mon,  mon  )
addTime( Year, year )
addTime( Hour, hour )
addTime( Min,  min  )
addTime( Sec,  sec  )

#undef addTime

void Tm::Register( HSQUIRRELVM v ) {
  Squirrelable<Tm,SquirrelableBase>::Register( v, "hlp",      Tm::hlp );
  Squirrelable<Tm,SquirrelableBase>::Register( v, "_get",     Tm::get );
  Squirrelable<Tm,SquirrelableBase>::Register( v, "addDays",  Tm::addDays );
  Squirrelable<Tm,SquirrelableBase>::Register( v, "addMons",  Tm::addMons );
  Squirrelable<Tm,SquirrelableBase>::Register( v, "addYears", Tm::addYears );
  Squirrelable<Tm,SquirrelableBase>::Register( v, "addHours", Tm::addHours );
  Squirrelable<Tm,SquirrelableBase>::Register( v, "addMins",  Tm::addMins );
  Squirrelable<Tm,SquirrelableBase>::Register( v, "addSecs",  Tm::addSecs );
}

// Acc

SQInteger Acc::hlp( HSQUIRRELVM h ) {
sq_pushstring( h, R"(    Acc help:
  Конструктор принимает строку.
  Функция sld() вычисляет сальдо:
  Если в качестве параметра функции sld() передано время, то сальдо вычисляется
на этот момент времени, в противном случае сальдо вычисляется на момент времени
periodEnds, который задан в переменной Cfg.periodEnds.
  Возвращает таблицу из двух элементов: булевого isD, которое истинно, если
сальдо дебетовое и элемента d для дебетового остатка или элемента c для
кредитового.
  Функция to() вычисляет обороты:
  Может вызываться без аргументов. В этом случае обороты вычисляются без
корреспонденции за перид времени, указанный в переменных Cfg.periodBegins и
Cfg.periodEnds.
  Если функции передан один аргумент, то он должен быть счетом в корреспонденции
с которым будут вычислены обороты.
  Если функции переданы два аргумента, то это должен быть период времени для
вычисления оборотов.
  Если функции переданы три аргумента, то первые два аргумента задают период
времени, а третий - корреспондирующий счет.
  Возвращает таблицу двух значений d и c.
)", -1 ); return 1;
}

SQInteger Acc::constructor( SqVm &v ) try {
  Str accName = v.get<const SQChar*>( 2 );
  it = bk::Acc::findByName( accName );
  if( it == bk::Acc::accsByName.end())
    throw std::runtime_error( "Sq:Acc: not found: " + accName );
  return 0;
} catch ( std::exception &e ) { return sq_throwerror( v, e.what()); }


SQInteger Acc::sld( HSQUIRRELVM h ) try {
  const auto &acc = *getO<Acc>( h ).it;
  auto &v = getV<SqVm>( h );
  Time t;
  switch( sq_gettop( v )) { // number of arguments
    case 1: t = cfg().periodEnds; break;
    case 2: t = v.get<Time>( 2, "Sq:Acc:sld: time" ); break;
    default: throw std::runtime_error( "Sq:Acc:sld: num args" );
  }
  // возвращает таблицу из двух элементов isD и элемента d для
  // дебетового остатка или c для кредитового
  Dd sld = En::saldo( acc, t ).s;
  sq_newtable( v ); v.newslot( "isD", sld.isD( acc ));
  if( sld.isD( acc ))
    v.newslot( "d", sld.d - sld.c );
  else
    v.newslot( "c", sld.c - sld.d );
  return 1;
} catch ( std::exception &e ) { return sq_throwerror( h, e.what()); }

SQInteger Acc::to( HSQUIRRELVM h ) try {
  const auto &acc = *getO<Acc>( h ).it;
  auto &v = getV<SqVm>( h );
  auto argc = sq_gettop( v ); // number of arguments
  Time t1 = cfg().periodBegins, t2 = cfg().periodEnds;
  const bk::Acc *corr = 0;

  if( argc > 2 ) { // в аргументах - период
    t1 = v.get<Time>( 2, "Sq:Acc:to: t1" );
    t2 = v.get<Time>( 3, "Sq:Acc:to: t2" );
  }
  if( argc == 2 || argc == 4 ) { // в аргументах - корреспондирующий счет
    if( sq_gettype( v, argc ) == OT_INSTANCE ) {
      if( Acc *p = dynamic_cast<Acc*>( v.get<SquirrelableBase*>( argc )))
        corr = &*p->it;
      else
        throw std::runtime_error( "Sq:Acc:to: correspond acc is not Acc class" );
    } else {
      Str corrName = v.get<const SQChar*>( argc );
      auto corrIt = bk::Acc::findByName( corrName );
      if( corrIt == bk::Acc::accsByName.end())
        throw std::runtime_error( "Sq:Acc:to: correspond acc not found: " + corrName );
      corr = &*corrIt;
    }
  }
  if( ! argc || argc > 4 ) throw std::runtime_error( "Sq:Acc:to: num args" );

  bk::To to = corr ? bk::En::turnover( acc, t1, t2, *corr )
                   : bk::En::turnover( acc, t1, t2 );
  // возвращает таблицу двух значений
  sq_newtable( v );
  v.newslot( "d", to.d ); v.newslot( "c", to.c );
  return 1;
} catch ( std::exception &e ) { return sq_throwerror( h, e.what()); }

void Acc::Register( HSQUIRRELVM v ) {
  Squirrelable<Acc,SquirrelableBase>::Register( v, "hlp",  Acc::hlp );
  Squirrelable<Acc,SquirrelableBase>::Register( v, "a",    Acc::a );
  Squirrelable<Acc,SquirrelableBase>::Register( v, "s",    Acc::s );
  Squirrelable<Acc,SquirrelableBase>::Register( v, "n",    Acc::n );
  Squirrelable<Acc,SquirrelableBase>::Register( v, "curr", Acc::curr );
  Squirrelable<Acc,SquirrelableBase>::Register( v, "type", Acc::type );
  Squirrelable<Acc,SquirrelableBase>::Register( v, "sld",  Acc::sld );
  Squirrelable<Acc,SquirrelableBase>::Register( v, "to",   Acc::to );
}

void registerSquirrels( SqVm &v ) {
  Squirrelable<SquirrelableBase> regSBase( v );
  Squirrelable<Tm>  regTm ( v );
  Squirrelable<Acc> regAcc( v );
}

}} // squirrels, bk

