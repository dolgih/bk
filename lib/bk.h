// bk.h

#pragma once

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <string>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <ctime>
#include <list>
#include <set>
#include <map>
#include <functional>
#include <algorithm>
#include <memory>

//#define SQUSEDOUBLE
#include <squirrel.h>
#include <sqstdio.h>
//#include <sqstdstring.h>
//#include <sqstdsystem.h>
#include <sqstdaux.h>

#include "utl.h"
#include "sq.h"
#include "sqBridge.h"
#include "cfg.h"


#pragma pack(1)

namespace bk {

struct Acc;
struct Dd { // двойное число
  double d, c;
  Dd() : d( 0.0 ), c( 0.0 ) { }
  Dd( double d, double c ) : d( d ), c( c ) { }
  Dd( const Dd &right ) : d( right.d ), c( right.c ) { }
  Dd &operator += ( const Dd &right ) { d += right.d; c += right.c; return *this; }
  Str str( const Acc &da, const Acc &ca ) const; // величина в двух валютах
  bool isD( const Acc &a ) const; // возвращает true для дебетового сальдо
  Str str( const Acc &a ) const; // сальдо
  Str tds( const Acc &a ) const; // сальдо
};

struct Acc {
  enum AccType { ActPass = 0, Active = 1, Passive = 2 };
  struct Options {
    unsigned currency:3;
    AccType type: 2;
    Options() : currency( 0 ), type( ActPass ) { }
    Options( unsigned short currency, AccType type = ActPass )
      : currency( currency ), type( type ) { }
  };

  const unsigned char a, s;
  const Str n;
  const Options o;

  Acc( unsigned char a, unsigned char s, const Str &n, Options o )
    : a( a ), s( s ), n( n ), o( o )
  {
    if( ! accs.insert( *this ).second ) throw Error( *this, "duplicate Account key" );
  }

  bool operator < ( const Acc &right ) const {
    if( a < right.a ) return true;
    if( right.a < a ) return false;
    return s < right.s;
  }
  bool operator == ( const Acc &right ) const {
    return a == right.a && s == right.s;
  }
  Str str() const {
    if( ! n.empty()) return n;
    Str o = n2s( a ); if( s ) o += "." + n2s( s );
    return o;
  }
  const char *currency() const;
  const char *type() const;

  struct AccCmpByName { bool operator() ( const Acc &left, const Acc &right ) const { return left.n < right.n; }};
  typedef std::set<Acc, AccCmpByName> AccsByName;
  static AccsByName accsByName; // план счетов, сортированный по названиям
  static auto findByName( const Str &n ) { return accsByName.find( Acc( n ) ); }

  struct AccCmp { bool operator() ( const Acc &left, const Acc &right ) const { return left < right; }};
  typedef std::reference_wrapper<const Acc> ConstRef;
  typedef std::set<ConstRef, AccCmp> Accs;
  static Accs accs; // индекс плана счетов по кодам
  static void initAccs( SqVm &v );

private:
  struct Error : std::runtime_error {
    Error( const Acc &acc, const char *what ) : std::runtime_error( "bk::Acc(" + acc.str() + "): " + what ) { }
  };
  Acc( const Str &n ) : a( 0 ), s( 0 ), n( n ), o( 0 ) /* только для поиска по названию */ { }
};

struct Sld; struct To;
struct En {
  const Time t;
  const Acc &d, &c;
  const Dd v; const Str rem;

  En( Time t, const Acc &d, const Acc &c, double v, const Str &rem = Str())
    : t( t ), d( d ), c( c ), v( v , v ), rem( rem ) {
      if( d.o.currency != c.o.currency )
        throw Error( *this, "different currencies accounts" );
  }
  En( Time t, const Acc &d, const Acc &c, double v, double v2, const Str &rem = Str())
    : t( t ), d( d ), c( c ), v( v, v2 ), rem( rem ) {
      if( d.o.currency == c.o.currency )
        throw Error( *this, "the same currency accounts" );
  }
  bool operator < ( const En &right ) const { return t < right.t; }
  Str str() const {
    return t.str() + "; " + d.str() + " << " + c.str() + "; " + v.str( d, c );
  }
  Str tds() const;

  typedef std::multiset<En> Ens;
  static Ens ens; // Журнал
  static void initEns( SqVm &v );

  static Ens::iterator upperBound( Time t ) { return En::ens.upper_bound( { t,
           *Acc::accs.cbegin() /* Здесь любое, сортировка только по времени */ } ); }

  static Sld saldo( const Acc &a, Time t = Time::now());
  static To turnover( const Acc &a, Time t1, Time t2 );
  static To turnover( const Acc &a, Time t1, Time t2, const Acc &corr );

private:
  struct Error : std::runtime_error {
    Error( const En &en, const char *what ) : std::runtime_error( "bk::En(" + en.str() + "): " + what ) { }
  };
  En( Time t, const Acc &a ) // Для поискового ключа
    : t( t ), d( a ), c( a ) { }
};

struct Sld {
  const Acc &a; Time t; Dd s;

  Sld( const Acc &a, Time t, Dd s ) : a( a ), t( t ), s( s ) { }
  bool operator < ( const Sld &right ) const {
    if( a < right.a ) return true;
    if( right.a < a ) return false;
    return t < right.t;
  }
  Str str( bool printTime ) const;
  Str tds( bool printTime ) const;

  typedef std::multiset<Sld> Slds;
  static Slds sSlds; // Сохраненные сальдо
  static void initSSlds( SqVm &v );

  static Sld findSaved( const Acc &a, Time t = Time::now()) {
    auto fnd = sSlds.upper_bound( { a, t, Dd() } );
    Sld empty( a, Time::millenium(), Dd());
    if( fnd == sSlds.begin()) return empty;
    fnd --;
    if( fnd->a == a ) return *fnd;
    return empty;
  }
};

struct To { // оборот вычисляется по принципу "t1 <= To <= t2"
  Time t1, t2; const Acc &a; double d, c;
  To( Time t1, Time t2, const Acc &a, double d = 0.0, double c = 0.0 )
    : t1( t1 ), t2( t2 ), a( a ), d( d ), c( c ) { }
};

} // bk

#pragma pack()

#include "squirrels.h"
