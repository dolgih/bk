// sqBridge.h

/** \brief Базовый класс для всех объектов, доступных в sq
*
* Любой наследуемый класс должен переопределить методы
*  typeOf() и className().
* И желательно переопределить и зарегистрировать статический метод hlp().
*/

namespace bk {

#define SquirrelableGetMethod(name, class, data) static SQInteger name(HSQUIRRELVM h) { \
    try { getV<SqVm>(h).push( getO<class>(h). data ); } \
    catch( std::exception &e ) { return sq_throwerror( h, e.what()); } \
    return 1; \
}
#define SquirrelableSetMethod(name, class, type, data) static SQInteger name(HSQUIRRELVM v) { \
    getO<class>(v). data = sq_get ## type(v, 2); \
    return 0; \
}

class SquirrelableBase {
public:
  static const char *typeOf() { return "SquirrelableBase"; }
  virtual const char *className() const { return typeOf(); }
  SquirrelableGetMethod( typeOf, SquirrelableBase, className());
  static SQInteger hlp( HSQUIRRELVM h ) {
    sq_pushstring( h, "Help function not implemented!", -1 ); return 1;
  }

  SquirrelableBase() { }
  virtual SQInteger constructor( SqVm & ) {
    /* must return 0 or sq_throwerror() */
    return 0;
  }
  virtual ~SquirrelableBase() { }

  template<class T> static T &getO( HSQUIRRELVM v ) {
    SquirrelableBase *up;
    if( SQ_SUCCEEDED( sq_getinstanceup( v, 1,
      reinterpret_cast<SQUserPointer*>( &up ), 0 )) && up
    )
      return dynamic_cast<T&>( *up );
    else
      throw std::runtime_error( "SquirrelableBase::getO: sq_getinstanceup" );
  }

  template<class T> static T &getV( HSQUIRRELVM v ) {
    return *static_cast<T*>( sq_getforeignptr( v ));
  }

  virtual Str tostring() const { return className(); }
  SquirrelableGetMethod( tostring, SquirrelableBase, tostring().data());

  static void Register( HSQUIRRELVM v );
};

template <class T, class Base = SquirrelableBase> class Squirrelable {
public:

  Squirrelable( HSQUIRRELVM v ) { Register( v ); T::Register( v ); }

  static void Register( HSQUIRRELVM v, Str name, SQFUNCTION func ) {
    sq_pushroottable( v );
    sq_pushstring( v, T::typeOf(), -1 );
    sq_get( v, -2 ); // TODO check result ?
    sq_pushstring( v, name.data(), name.size());
    sq_newclosure( v, func, 0 );
    sq_newslot( v, -3, SQFalse );
    sq_pop( v, 2 );
  }

  static void Register( HSQUIRRELVM v ) {
    sq_pushroottable( v );

    sq_pushstring( v, T::typeOf(), -1 );

    if( typeid( Base ) != typeid( T )) {
      sq_pushstring( v, Base::typeOf(), -1 );
      sq_get( v, -3 ); // TODO check result ?
      sq_newclass( v, 1 );
    } else
      sq_newclass( v, 0 );

    sq_setclassudsize( v, -1, sizeof( T ));
    sq_pushstring( v, "constructor", -1 );
    sq_newclosure( v, createInstance, 0 );
    sq_newslot( v, -3, false ); // Add the constructor method

    sq_newslot( v, -3, SQFalse ); // Add the class

    sq_poptop( v );
  }

private:

  static SQInteger createInstance( HSQUIRRELVM v ) {
    SqVm &vm = *static_cast<SqVm*>( sq_getforeignptr( v ));
    T *p;
    sq_getinstanceup( v, 1, reinterpret_cast<SQUserPointer*>( &p ), 0 );
    try { new ( p ) T; }
    catch( std::exception &e ) { return sq_throwerror( v, e.what()); }
    sq_setreleasehook( v, 1, deleteInstance );
    return p->constructor( vm );
  }

  static SQInteger deleteInstance( SQUserPointer ptr, SQInteger size ) {
    static_cast<T *>( ptr )->~T();
    return 0;
  }
};


inline void SquirrelableBase::Register( HSQUIRRELVM v ) {
  Squirrelable<SquirrelableBase>::Register( v, "_typeof", SquirrelableBase::typeOf );
  Squirrelable<SquirrelableBase>::Register( v, "_tostring", SquirrelableBase::tostring );
  Squirrelable<SquirrelableBase>::Register( v, "hlp", SquirrelableBase::hlp );
}

} // bk

