// cfg.cpp

#include "bk.h"

namespace bk {

struct EnvInt {
  EnvInt( const char *envVar, long def ) : i( def ) {
    if( const char *val = getenv( envVar )) {
      if( ! strcasecmp( val, "true" )) i = 1;
      else if( ! strcasecmp( val, "false" )) i = 0;
      else {
        char *end; i = strtol( val, &end, 10 );
        if( *end ) throw std::runtime_error( utl::f(
            "EnvInt({1}) bad value: \"{2}\"", envVar, val ));
      }
    }
  }
  operator long() const { return i; }
private:
  long i;
};

struct EnvTime : Time {
  EnvTime( const char *envVar, Time def ) : Time( def ) {
    if( const char *val = getenv( envVar )) t = Time( val );
  }
};

struct EnvStr : Str {
  EnvStr( const char *envVar, const Str &def ) : Str( def )
  { if( const char *val = getenv( envVar ) ) assign( val ); }
};

void convertPeriodToHumanly ( Time &t1, Time &t2 ) {
  std::tm tm = t1;
  t1 = static_cast<std::time_t>( Time( tm.tm_mday, tm.tm_mon + 1,
    tm.tm_year - 100 )) - 1 /* минус одна секунда */;
          tm = t2;
  t2 = static_cast<std::time_t>( Time( tm.tm_mday, tm.tm_mon + 1,
    tm.tm_year - 100 )) + 86400 - 1 /* плюс сутки минус одна секунда */;
};

const char       Cfg[] = "Cfg",
             verbose[] = "verbose",
       periodHumanly[] = "periodHumanly",
   reportSavedSaldos[] = "reportSavedSaldos",
       reportEntries[] = "reportEntries",
        reportSaldos[] = "reportSaldos",
      reportTurnover[] = "reportTurnover",
         reportChess[] = "reportChess",
        periodBegins[] = "periodBegins",
          periodEnds[] = "periodEnds",
      inputDirectory[] = "inputDirectory",
     outputDirectory[] = "outputDirectory",
   outFileNameSuffix[] = "outFileNameSuffix",
          scriptFile[] = "scriptFile";

Config::Config( SqVm &v, const Str& fName ) : SqFile( v, fName ) { }

void Config::run() {
  // Создание констант Log*
  sq_pushconsttable( v ); v.push( "LogLevel" );
  sq_newtable( v );
  v.newslot( "Error",   LogError );
  v.newslot( "Warning", LogWarning );
  v.newslot( "Info",    LogInfo );
  v.newslot( "Debug",   LogDebug );
  sq_newslot( v, -3, 0 ); sq_poptop( v /* consttable */ );

  // Создание переменных
  sq_pushroottable( v );
  v.push( bk::Cfg ); sq_newtable( v );

  v.newslot( bk::verbose,           verbose );
  v.newslot( bk::reportSavedSaldos, false );
  v.newslot( bk::reportEntries,     false );
  v.newslot( bk::reportSaldos,      true  );
  v.newslot( bk::reportTurnover,    true  );
  v.newslot( bk::reportChess,       true  );
  v.newslot( bk::periodHumanly,     true  );
  v.newslot( bk::periodBegins,      Time::millenium().str());
  v.newslot( bk::periodEnds,        Time::now().str());
  v.newslot( bk::inputDirectory,    "./in/" );
  v.newslot( bk::outputDirectory,   "./out/" );
  v.newslot( bk::outFileNameSuffix, Str());
  v.newslot( bk::scriptFile,        "bk++.nut" );

  sq_newslot( v, -3, 0 ); sq_poptop( v /* roottable */ );

  SqFile::run();

  // Чтение переменных
  sq_pushroottable( v ); v.push( bk::Cfg ); sq_get( v, -2 );

#define TOENV( n ) "BK_" # n
#define GETSTRPAR(  n ) n = EnvStr ( TOENV( n ), v.find<const SQChar*>( bk::n, bk::n ));
#define GETTIMEPAR( n ) n = EnvTime( TOENV( n ), v.find<const SQChar*>( bk::n, bk::n ));
#define GETINTPAR ( n ) n = EnvInt ( TOENV( n ), v.find<SQInteger>( bk::n, bk::n ));
#define GETBOOLPAR( n ) n = EnvInt ( TOENV( n ), v.find<SQBool>( bk::n, bk::n ));
  verbose = (LogLevel)(long)EnvInt( TOENV( verbose ), v.find<SQInteger>( bk::verbose, bk::verbose ));
  GETBOOLPAR( periodHumanly );
  GETBOOLPAR( reportSavedSaldos );
  GETBOOLPAR( reportEntries );
  GETBOOLPAR( reportSaldos );
  GETBOOLPAR( reportTurnover );
  GETBOOLPAR( reportChess );
  GETTIMEPAR( periodBegins );
  GETTIMEPAR( periodEnds );
  if( periodBegins == Time::millenium()) periodBegins = periodEnds - 2678400 /* 31 день*/;
  if( periodHumanly ) convertPeriodToHumanly( periodBegins, periodEnds );
  GETSTRPAR ( inputDirectory );
  GETSTRPAR ( outputDirectory );
  GETSTRPAR ( outFileNameSuffix );
  GETSTRPAR ( scriptFile );

  // создадим объекты Cfg.periodBegins и Cfg.periodEnds класса Tm
  v.push( bk::periodBegins ); v.push( periodBegins.str());
  v.createInstance( squirrels::Tm::typeOf(), 1 );
  sq_set( v, -3 );
  v.push( bk::periodEnds ); v.push( periodEnds.str());
  v.createInstance( squirrels::Tm::typeOf(), 1 );
  sq_set( v, -3 );

  sq_pop( v, 2 /* Cfg, roottable */ );
}


std::unique_ptr<Config> config;
void initCfg( SqVm &v, const Str& fName ) {
  config = std::unique_ptr<Config>( new Config( v, fName ));
  config->run();
}
const Config &cfg() { return *config; }

} //bk

