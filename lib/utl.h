// utl.h

#pragma once

typedef std::string Str;

namespace bk {
namespace utl {

/** Converts value to Str. */
template<class T, int maxSize = 32> inline Str n2s( T val, const char *format ) {
  char out[ maxSize ]; snprintf( out, sizeof out, format, val ); return out;
}

/** Converts unsigned to Str. */
inline Str n2s( unsigned val ) { return n2s( val, "%u" ); }
/** Converts signed to Str. */
inline Str n2s( int val ) { return n2s( val, "%i" ); }
/** Converts unsigned long to Str. */
inline Str n2s( unsigned long val ) { return n2s( val, "%lu" ); }
/** Converts long to Str. */
inline Str n2s( long val ) { return n2s( val, "%li" ); }
/** Converts unsigned long long to Str. */
inline Str n2s( unsigned long long val ) { return n2s( val, "%llu" ); }
/** Converts long long to Str. */
inline Str n2s( long long val ) { return n2s( val, "%lli" ); }
/** Converts double to Str. */
inline Str n2s( double val ) { return n2s( val, "%.2f" ); }


struct Time {
  Time() : t( 0 ) { }
  Time( unsigned char d, unsigned char m, unsigned char y ) { t = Time( d, m, y, 0, 0 ); }
  struct Error : std::runtime_error {
    Error( const char *what ) : std::runtime_error( Str( "bk::Time: " ) + what ) { }
  };
  void verify( unsigned d, unsigned m, unsigned y, unsigned h, unsigned min, unsigned s ) {
    if( ! d || d > 31 || ! m || m > 12 || y > 99
      || h > 23 || min > 59 || s > 59 )
        throw Error( "bad time format");
  }
  Time( unsigned char d, unsigned char m, unsigned char y, unsigned char h, unsigned char min, unsigned char s = 0 ) {
    verify( d, m, y, h, min, s );
    std::tm tm = { s, min, h, d, m - 1, y + 100, 0, 0, 0 };
    t = std::mktime( &tm );
  }
  Time( const char *str ) {
    unsigned d = 0, m = 0, y = 0, h = 0, min = 0, s = 0;
    unsigned char c = sscanf( str, "%u.%u.%u %u:%u:%u", &d, &m, &y, &h, &min, &s );
    if( c != 3 && c < 5 ) throw Error( "number of arguments");
    verify( d, m, y, h, min, s );
    t = Time( d, m, y, h, min, s );
  }
  Time( std::time_t t ) : t( t ) { }
  Time( std::tm tm ) { t = mktime( &tm ); }
  bool operator < ( const Time &right ) const { return t < right.t; }
  operator std::time_t() const { return t; }
  operator std::tm() const { std::tm tm; localtime_r( &t, &tm ); return tm; }
  std::tm gmt() const { std::tm tm; gmtime_r( &t, &tm ); return tm; }
  static Time millenium() { return Time( Time( 1, 1, 0 )); }
  static Time now() { return Time( std::time( 0 )); }
  Str str() const {
    std::tm tm = *this;
    char o[ 64 ]; strftime( o, sizeof o, "%d.%m.%y", &tm );
    if( tm.tm_hour || tm.tm_min || tm.tm_sec ) strftime( o + 8, sizeof o - 8, " %H:%M:%S", &tm );
    return o;
  }
protected:
  std::time_t t;
};

/** Format string */
class F { public:
  F( const Str &fmt ) : theFormat( fmt ) { }
  F &operator()( const Str &v )          { store[ store.size() ] = v; return *this; }
  F &operator()( const char *v )         { store[ store.size() ] = v; return *this; }
  template<class T> F &operator()( T v ) { store[ store.size() ] = n2s( v ); return *this; }
  template<class T> F &operator%( const T &v ) { return operator()( v ); }
  operator Str() const;
  Str str() const { return operator Str(); }
protected:
  Str theFormat;
  std::map<unsigned char,Str> store;
};
template<typename ...Ts> Str f( const char *fmt, const Ts &...args ) {
  F o( fmt );
  int dummy[ sizeof...(Ts) + 1 ] = { ( o( args ), 0 )... }; dummy[ 0 ] ++;
  return o;
};
template<typename ...Ts> Str f( Str fmt, const Ts &...args ) {
  return f( fmt.data(), args... );
};

inline std::ostream &operator << ( std::ostream &stream, const F &f ) {
  return stream << (Str)f;
}

/**
  Returns a new string in which all occurrences of a specified string
  in the current instance are replaced with another specified string.
*/
Str replace( Str s, Str pattern, Str repl );

/** Removes spaces from start. */
inline Str ltrim( Str s ) {
  s.erase( s.begin(), std::find_if( s.begin(), s.end(),
      std::not1( std::ptr_fun<int, int>( std::isspace ))));
  return s;
}

/** Removes spaces from end. */
inline Str rtrim( Str s ) {
  s.erase( std::find_if( s.rbegin(), s.rend(),
      std::not1( std::ptr_fun<int, int>( std::isspace ))).base(), s.end());
  return s;
}

/** Removes spaces from both ends. */
inline Str trim( Str s ) { return ltrim( rtrim( s )); }

} // utl

using utl::n2s;
using utl::Time;

} // bk

