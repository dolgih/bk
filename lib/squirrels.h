// squirrels.h

#pragma once

namespace bk {
namespace squirrels {

class Tm : public SquirrelableBase {
  std::tm t;
public:
  static const char *typeOf() { return "Tm"; }
  const char *className() const override { return typeOf(); }
  static SQInteger hlp( HSQUIRRELVM h );

  SQInteger constructor( SqVm &v ) override;
  operator const std::tm &() const { return t; }

  Str tostring() const override { return static_cast<Time>( t ).str(); }
  static SQInteger get( HSQUIRRELVM h );
  static SQInteger addDays ( HSQUIRRELVM h );
  static SQInteger addMons ( HSQUIRRELVM h );
  static SQInteger addYears( HSQUIRRELVM h );
  static SQInteger addHours( HSQUIRRELVM h );
  static SQInteger addMins ( HSQUIRRELVM h );
  static SQInteger addSecs ( HSQUIRRELVM h );

  static void Register( HSQUIRRELVM v );
};

class Acc : public SquirrelableBase {
  bk::Acc::AccsByName::const_iterator it;
public:
  static const char *typeOf() { return "Acc"; }
  const char *className() const override { return typeOf(); }
  static SQInteger hlp( HSQUIRRELVM h );

  SQInteger constructor( SqVm &v ) override;

  Str tostring() const override { return it->str(); }
  SquirrelableGetMethod( a, Acc, it->a );
  SquirrelableGetMethod( s, Acc, it->s );
  SquirrelableGetMethod( n, Acc, it->n.data());
  SquirrelableGetMethod( curr, Acc, it->currency());
  SquirrelableGetMethod( type, Acc, it->type());

  static SQInteger sld( HSQUIRRELVM v );
  static SQInteger to( HSQUIRRELVM v );

  static void Register( HSQUIRRELVM v );
};


extern void registerSquirrels( SqVm &v );

}} // squirrels, bk

