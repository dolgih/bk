// s.cpp

#include "bk.h"

namespace bk {

Sld::Slds Sld::sSlds;

struct Saldos : ReadSq {
  Saldos( SqVm &v )
    : ReadSq( v, cfg().inputDirectory + "Saldos.in", "Saldos" ), v( v ) { }
  void run() override {
    ReadSq::run();
    if( cfg().verbose >= Config::LogInfo ) {
      std::cerr << "Values list:\n";
      for( const auto &s: Sld::sSlds )
        std::cerr << s.str( true ) << '\n';
    }
  }
  void line() override {
    // here -1 is the value and -2 is the key
    if( sq_gettype( v, -1 ) != OT_ARRAY ) error( "not array: Saldos" );
    // array
    if( sq_getsize( v, -1 ) < 3 ) error( "too little array" );
    Str a; Time t; Dd s;

    a = v.find<const SQChar*>( 0, "Sld::a" );
    t = v.find<Time>( 1, "Sld::t" );

    auto ap = Acc::findByName( a );
    if( ap == Acc::accsByName.end()) error( "account not found" );

    if( ap->o.type == Acc::Active )
      s.d = v.find<SQFloat>( 2, "Sld::s" );
    else if( ap->o.type == Acc::Passive )
      s.c = v.find<SQFloat>( 2, "Sld::s" );
    else {
      if( v.findType( 2 ) != OT_ARRAY ) error( "not array: Sld::s" );
      v.push( 2 ); sq_get( v, -2 );
      s.d = v.find<SQFloat>( 0, "Sld::s.d" );
      s.c = v.find<SQFloat>( 1, "Sld::s.c" );
      sq_poptop( v /* array */ );
    }

    Sld::sSlds.emplace( *ap, t, s );
  }
protected:
  SqVm &v;
};

void Sld::initSSlds( SqVm &v ) { Saldos( v ).run(); }

} // bk

