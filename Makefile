
CXXFLAGS+=-I lib -I/usr/include/squirrel3 -Wall -g2

LDFLAGS+=-lsquirrel3 -lsqstdlib3

DIRS += lib

SOURCES = $(foreach DIR,$(DIRS),$(wildcard $(DIR)/*.cpp))
HEADERS = $(foreach DIR,$(DIRS),$(wildcard $(DIR)/*.h))
OBJS = ${SOURCES:.cpp=.o}

Debug: all

all: bk

bk:	${OBJS}
	${CXX} -o $@ $^ $(LDFLAGS)

# precompiled headers
lib/bk.h.gch:	$(HEADERS)
	${CXX} ${CXXFLAGS} lib/bk.h -o $@

${OBJS}: lib/bk.h.gch
# end of precompiled headers

cleanDebug: clean

.PHONY:	clean
clean:
	rm -f ${OBJS} bk lib/bk.h.gch

install: bk
	cp bk ~/.local/bin && strip ~/.local/bin/bk

AD1=-n -b -v -q=f -j=2 -H -sl -cx bk++ \
  "lib/*.cpp" "lib/*.h" Makefile bk.cbp bk++.cfg bk++.nut .gitignore \
  codelite/bk.project codelite/Makefile

.PHONY:	save
save:
	ad1 -u $(AD1)

.PHONY:	cmp
cmp:
	ad1 $(AD1)

