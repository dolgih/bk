// bk++ Squirrel program (debug version)


function printAccount( a ) {
  print( "Acc n="+a.n()+"; a="+a.a()+"; s="+a.s()+"; typeof=" + typeof( a )
    +"; curr="+a.curr()+"; type="+a.type()+"\n" );
}

foreach( k, a in Accounts ) printAccount( a );

local a = Acc( cash ); print( "\n" ); printAccount( a );
print( "сальдо на 8.5.19="+a.sld( "8.5.19" ).d+"\n" );
print( "сальдо на [1,4,19]="+a.sld( [1,4,19] ).d+"\n" );
print( "сальдо без даты (на periodEnds)="+a.sld().d+"; periodEnds="
  +Cfg.periodEnds+"\n" );

a = Accounts.bank; printAccount( a );
print( "\n" );
print( "оборот без дат="+a.to().d+"; "+a.to().c+"\n" );
local to = a.to([1,4,19],"9.5.19",Accounts.bak);
print( "оборот ([1,4,19],\"9.5.19\",Accounts.bak)="+to.d+"; "+to.c+"\n" );

// class Tm
local tm = Tm( Cfg.periodEnds )
print( "\nd="+tm.d+"  m="+tm.mo+"  y="+tm.YE+"  h="+tm.H+"  mi="+tm.mi
 +"  s="+tm.seC+"  tm="+tm+"\n" )
local tm2 = Tm(tm)
print( "tm2="+tm2+"  typeof(tm2)="+typeof(tm2)+"\n" )

// Tm::add...()
print( "tm2.addMons(-1)="+tm2.addMons(-1)+"\n" );

// Help
print( "\nHelp\n\n"+Tm.hlp()+"\n" );
print( Accounts.cash.hlp()+"\n" );

// Сальдо на Cfg.periodEnds для [cash, bank, bak]
print( "\nСальдо на " + Cfg.periodEnds + " :\n" );
foreach( n in [cash, bank, bak] ) { print( n+"\t= "+Accounts[n].sld().d+"\n" ); }

